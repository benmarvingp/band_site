<?php
/*
You can overwrite any functions here.
Make sure that the function you want to overwrite is wrapped in the parent theme like this :

if ( ! functions_exists( 'the_function_name' ) ) :
	function the_function_name(){}
endif;

----------
Example :

if ( ! functions_exists( 'wolf_custom_function' ) ) :

function wolf_custom_function() {
	//your fancy code here
}

endif;
*/

//-------------------------------------------------------------------------

/**
 * You can overwrite the admin menu settings and update notification constants in this function
 */

// Display the theme update notice
define( 'WOLF_UPDATE_NOTICE', true );

// Display the link to the support forum
define( 'WOLF_SUPPORT_PAGE', true );

// Enable the customizer
define( 'WOLF_ENABLE_CUSTOMIZER', true );

// Have fun!